package MMBSmoke.MMBSmoke.steps;

import java.io.File;

//import org.junit.After;

import MMBSmoke.MMBSmoke.pages.*;
import MMBSmoke.MMBSmoke.utils.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

//SelectUtility class extend
public class SelectSteps extends TestBase	 {
	
	LoginPage loginPage;
	HomePage homePage;
	ClaimsDetails claimDetails;
	MainMenuOther mainmenuOther;
	MainMenuCoverage mainmenuCoverage;
	
	@Given("^user launched the browser$")
	public void user_launched_the_browser() throws Throwable {
			initialize();
			openBrowser();
	}

	
	@Then("^user enter username and password$")
	public void enter_username_and_password() throws Throwable {
		loginPage = new LoginPage();
		loginPage.login();
	}

	@Then("^user is on the employee home page$")
	public void employee_portal_Home_Page () throws Throwable {
		homePage = new HomePage();
		homePage.employeeHomePage();
	}
	
	@Then("^emailaddress link should display$")
	public void emailaddress_link_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.emailAddress();
		
	  }

	@Then("^contact Insurer link should display$")
	public void contact_Insurer_link_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.contactInsurer();
	    
	}
	
	@Then("^user click on the contact Insurer link$")
	public void user_click_on_the_contact_Insurer_link() throws Throwable {
		homePage = new HomePage();
		homePage.clickcontactInsurer();
	}
	
	
	@Then("^your claims table should display$")
	public void your_claims_table_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.yourClaimTable();
	}

	@Then("^get a claim form link should display$")
	public void get_a_claim_form_link_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.downloadClaimText();
	}

	@Then("^sell all claims link should display$")
	public void sell_all_claims_link_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.allClaimText();
	}

	@Then("^see pending claims link should display$")
	public void see_pending_claims_link_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.pendingClaims();
	}
	
	@When("^user clicks on claim number$")
	public void user_clicks_on_claim_number() throws Throwable {
	    claimDetails = new ClaimsDetails();
	    claimDetails.clickClaimNumber();
	}

	@Then("^claim details page should display$")
	public void claim_details_page_should_display() throws Throwable {
		claimDetails = new ClaimsDetails();
	    claimDetails.validateClaimDetailsPage();
	}


	@Then("^Insurer Contacts page should display$")
	public void insurer_Contacts_page_should_display() throws Throwable {
		homePage = new HomePage();
		homePage.validateContactInsurerPage();
	}
	
	@And("^view details link shouls display$")
	public void view_detail_link() throws Throwable {
		homePage = new HomePage();
		homePage.viewDetailLink();
	}
	
	@Then("^user click on the view details link$")
	public void click_view_detail_link() throws Throwable {
		homePage = new HomePage();
		homePage.clickviewDetail();
	}
	
	
	
	@Then("^personal detail text should display$")
	public void personal_detail_text() throws Throwable {
		homePage = new HomePage();
		homePage.personalDetailText();
	}
	
	@Then("^user click on the main menu button$")
	public void user_click_on_the_main_menu_button() throws Throwable {
	   mainmenuCoverage = new MainMenuCoverage();
	   mainmenuCoverage.clickMainMenuLink();
	}

	@When("^user click on the useful link button$")
	public void user_click_on_the_useful_link_button() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.clickusefulLink();
	}

	@Then("^useful link page should display$")
	public void useful_link_page_should_display() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.validateusefulLinkPage();
	}

	@Then("^user click on the close button$")
	public void user_click_on_the_close_button() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.closeButton();
	}


	@When("^user click on the news link button$")
	public void user_click_on_the_news_link_button() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.clicknewsLink();
	}

	@Then("^news link page should display$")
	public void news_link_page_should_display() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.validatenewsPage();
	}

	@When("^user click on the survey link button$")
	public void user_click_on_the_survey_link_button() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.clicksurveysLink();
	}

	@Then("^survey link page should display$")
	public void survey_link_page_should_display() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.validatesurveyLinkPage();
	}
	
	@Then("^user click on the news close button$")
	public void user_click_on_the_news_close_button() throws Throwable {
		 mainmenuOther = new MainMenuOther();
		   mainmenuOther.closeButtonNews();
	}
	
	
	@When("^user click on the dental link button$")
	public void user_click_on_the_dental_link_button() throws Throwable {
	    mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.clickDental();
	}

	@Then("^dental page should display$")
	public void dental_page_should_display() throws Throwable {
		 	mainmenuCoverage = new MainMenuCoverage();
		    mainmenuCoverage.validatedentalPage();
	}

	@When("^user click on the inpatient link button$")
	public void user_click_on_the_inpatient_link_button() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.clickInpatient();
	}

	@Then("^inpatient page should display$")
	public void inpatient_page_should_display() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.validateinpatientPage();
	}

	@When("^user click on the maternity link button$")
	public void user_click_on_the_maternity_link_button() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.clickMaternity();
	}

	@Then("^maternity page should display$")
	public void maternity_page_should_display() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.validatematernityPage();
	}

	@When("^user click on the list of coverages link button$")
	public void user_click_on_the_list_of_coverages_link_button() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.clickListOfCoverages();
	}

	@Then("^list of coverages page should display$")
	public void list_of_coverages_page_should_display() throws Throwable {
		mainmenuCoverage = new MainMenuCoverage();
	    mainmenuCoverage.validatelistofcoveragePage();
	}

	
	@Then("^close browser$")
	public void closeBrowser() throws Throwable {
		driver.quit();
		//Logger.info("All the testcases has been completed hence closing the browser!!!");
	}
	
}
