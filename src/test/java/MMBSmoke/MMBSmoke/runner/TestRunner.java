package MMBSmoke.MMBSmoke.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "features/MMBCoverageLink.feature"	,
glue={"MMBSmoke.MMBSmoke.steps"} ,  
plugin={"pretty" , "html:target/cucumber-html-report"} , 
//plugin={"pretty" , "json:target/report.json"} ,
monochrome =true , strict= true, dryRun= false)

public class TestRunner {
	
}