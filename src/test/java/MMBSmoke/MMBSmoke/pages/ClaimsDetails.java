package MMBSmoke.MMBSmoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import MMBSmoke.MMBSmoke.utils.CommonFunctions;
import MMBSmoke.MMBSmoke.utils.TestBase;

public class ClaimsDetails extends TestBase {
	
	
	
	
	//page factory- OR:
	@FindBy(xpath = "//*[contains(@id, '_gvEmployeeClaimList_lblClaimNumber_0')]")
	WebElement claimNumber;
		
	
	@FindBy(xpath = "//*[@id='printDivContent']/fieldset/div[3]/div[1]/div/h3")
	WebElement claimDetails;
		
		
		
		//Initializing the page objects
		public ClaimsDetails(){
			PageFactory.initElements(driver, this);
		}
		
		
		
		public void clickClaimNumber(){
			try {
				clickButton(claimNumber);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public void validateClaimDetailsPage(){
			CommonFunctions.Validate(claimDetails, "Claim Details");
		}

}
