package MMBSmoke.MMBSmoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import MMBSmoke.MMBSmoke.utils.CommonFunctions;
import MMBSmoke.MMBSmoke.utils.TestBase;

public class LoginPage extends TestBase {
	
	
	
	//Page Factory- OR:
	@FindBy(xpath = "//input[contains(@id, '_MMBPortal_txtUsername')]")
	WebElement username;
	
	@FindBy(xpath = "//input[contains(@id, '_Login_Login_MMBPortal_txtPassword-clone')]")
	WebElement password;
	
	@FindBy(xpath = "//*[contains(@id, '_Login_Login_MMBPortal_txtPassword')]")
	WebElement password1;
	
	@FindBy(xpath = "//input[contains(@id, '_MMBPortal_cmdLogin')]")
	WebElement loginButton;
	
	
	//@FindBy(xpath= "//a[contains(@id, 'loginButton')]")
	
	@FindBy(xpath= "//span[contains(text(), 'Sign In')]")
	WebElement signInButton;
	
	
	public LoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	
	public HomePage login() throws InterruptedException{
	
		clickButton(signInButton);
		Thread.sleep(1000);
		CommonFunctions.enterText(username, "0180105");
		
		clickButton(password);
		CommonFunctions.enterText(password1, "Pass123$");
		
		clickButton(loginButton);
		
		
		
		
	return new HomePage();
		
	}

}
