package MMBSmoke.MMBSmoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import MMBSmoke.MMBSmoke.utils.CommonFunctions;
import MMBSmoke.MMBSmoke.utils.TestBase;

public class HomePage extends TestBase {

	//page factory- OR:
	@FindBy(xpath="//*[@class='MainMenu_MenuBar']/div")
	WebElement homePage;
	
	@FindBy(xpath="//*[@class='MainMenu_MenuBar']//*[contains(text(), 'Claims')]")
	WebElement claimsButton;
	
	@FindBy(xpath = "//input[contains(@id, '_Login_ctrlLoginType_btnEmployee')]")
	WebElement loginAsEmployee;
	
	@FindBy(xpath = "//*[contains(@id, '_lblPersonalDetails')]")
	WebElement employeePersonalDetails;
	
	@FindBy(xpath = "//*[contains(@id, '_lblViewDetailsHeader')]")
	WebElement viewDetails;
	
	@FindBy(xpath = "//*[@id='odsContent']/div[2]/fieldset/div/div[2]/div[1]/div/h3")
	WebElement personalDetailsText;
	
	
	@FindBy(xpath = "//*[contains(@id, '_lblYourClaim')]")
	WebElement YourClaim;
	
	
	
	@FindBy(xpath = "//*[contains(@id, '_lblDownloadClaim')]")
	WebElement DownloadClaim;
	
	
	@FindBy(xpath = "//*[contains(@id, '_lblAllClaim')]")
	WebElement AllClaim;
	
	@FindBy(xpath = "//*[contains(@id, '_lblPendingClaims')]")
	WebElement PendingClaims;
	
	
	
	@FindBy(xpath = "//*[@id='odsContent']/div[1]/div/h1")
	WebElement InsurerContactPage;
	
	@FindBy(xpath = "//*[contains(@id, '_lblInsurerContactLinkDetail')]")
	WebElement InsurerContact;
	
	@FindBy(xpath = "//*[contains(@id, '_contacUsEmail')]")
	WebElement emailAddress;
	
	
	//Initializing the page objects
	public HomePage(){
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void employeeHomePage() throws InterruptedException {
		clickButton(loginAsEmployee);
		Thread.sleep(5000);
		
		CommonFunctions.Validate(employeePersonalDetails, "Personal Details:");
		
	//	return new ClaimsAdminPage();
		
	}
	
	public void viewDetailLink(){
		
		CommonFunctions.Validate(viewDetails, "View Details");
		
	}
	
	public void clickviewDetail() throws InterruptedException{
		clickButton(viewDetails);
		
	}
	
	public void emailAddress(){
		CommonFunctions.Validate(emailAddress, "emailaddress.ID@company.com");
	}
	
	public void contactInsurer(){
		CommonFunctions.Validate(InsurerContact, "Contact Insurer");
		
		
	}
	
	public void clickcontactInsurer(){
		try {
			clickButton(InsurerContact);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void yourClaimTable(){
		CommonFunctions.Validate(YourClaim, "Your Claims");
	}
	
	public void downloadClaimText(){
		CommonFunctions.Validate(DownloadClaim, "Get a Claim Form");
	}
	
	public void allClaimText(){
		CommonFunctions.Validate(AllClaim, "See All Claims");
	}
	
	public void pendingClaims(){
		CommonFunctions.Validate(PendingClaims, "See Pending Claims");
	}
	
	public void validateContactInsurerPage(){
		CommonFunctions.Validate(InsurerContactPage, "Insurer Contacts");
	}
	
	
	public void personalDetailText(){
		CommonFunctions.Validate(personalDetailsText, "Personal Details");
	}
	
}
