package MMBSmoke.MMBSmoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import MMBSmoke.MMBSmoke.utils.CommonFunctions;
import MMBSmoke.MMBSmoke.utils.TestBase;

public class MainMenuCoverage extends TestBase{
	
	
	@FindBy(xpath = "//*[@id='dnn_evoHeader_menuEmployee_EmpHeader']/div[1]/div/nav/div/a/i")
	WebElement mainMenu;
	
	//*[@id="dnn_evoHeader_menuEmployee_divMain"]/dl[1]/dd[1]/span/a
	@FindBy(xpath = "//a[text()='DENTAL']")
	WebElement dentalLink;
	
	@FindBy(xpath = "//a[text()='INPATIENT']")
	WebElement inpatientLink;
	
	@FindBy(xpath = "//a[text()='MATERNITY']")
	WebElement maternityLink;
	
	
	@FindBy(xpath = "//a[text()='List of Coverages']")
	WebElement listOfCoveragesLink;
	
	@FindBy(xpath = "//*[@id='odsContent']/div[2]/fieldset/div/div[2]/div[1]/div/div/ul/li/div[1]/span[2]")
	WebElement dentalLinkPage;
	
	
	@FindBy(xpath = "//*[@id='odsContent']/div[2]/fieldset/div/div[2]/div[1]/div/div/ul/li/div[1]/span[2]")
	WebElement inpatientLinkPage;
	
	
	@FindBy(xpath = "//*[@id='odsContent']/div[2]/fieldset/div/div[2]/div[1]/div/div/ul/li/div[1]/span[2]")
	WebElement maternityLinkPage;
	
	
	@FindBy(xpath = "//h3[text()='List Of Coverages']")
	WebElement listOfCoveragesLinkPage;
	
	
	
	
	//Initializing the page objects
		public MainMenuCoverage(){
			PageFactory.initElements(driver, this);
		}
		
		
		public void clickMainMenuLink() throws InterruptedException{
			CommonFunctions.staticwait(1);
			clickButton(mainMenu);
			
		}
		
		public void clickDental() throws InterruptedException{
			//clickButton(mainMenu);
			//Thread.sleep(5000);
			CommonFunctions.staticwait(1);
			clickButton(dentalLink);
		}
		
		
		public void clickInpatient() throws InterruptedException{
			//clickButton(mainMenu);
			//Thread.sleep(5000);
			CommonFunctions.staticwait(1);
			clickButton(inpatientLink);
		}
		
		public void clickMaternity() throws InterruptedException{
			//clickButton(mainMenu);
			//Thread.sleep(5000);
			CommonFunctions.staticwait(1);
			clickButton(maternityLink);
		}
		
		public void clickListOfCoverages() throws InterruptedException{
			//clickButton(mainMenu);
			//Thread.sleep(5000);
			CommonFunctions.staticwait(1);
			clickButton(listOfCoveragesLink);
		}
		
		public void validatedentalPage(){
			CommonFunctions.Validate(dentalLinkPage, "DENTAL - Dental ");
		}
		
		public void validateinpatientPage(){
			CommonFunctions.Validate(inpatientLinkPage, "INPATIENT - Group Hospital &amp; Surgical ");
		}
		
		public void validatematernityPage(){
			CommonFunctions.Validate(maternityLinkPage, "MATERNITY - Maternity Benefit ");
		}
		
		public void validatelistofcoveragePage(){
			CommonFunctions.Validate(listOfCoveragesLinkPage, "List Of Coverages");
		}
		
		
		
		
		
		
		
}
