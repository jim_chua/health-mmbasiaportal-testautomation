package MMBSmoke.MMBSmoke.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import MMBSmoke.MMBSmoke.utils.*;

public class MainMenuOther extends TestBase{
	
	
	@FindBy(xpath = "//*[@id='dnn_evoHeader_menuEmployee_EmpHeader']/div[1]/div/nav/div/a/i")
	WebElement mainMenu;
	
	@FindBy(xpath = "//a[text()='Useful Links']")
	WebElement usefulLink;
	
	@FindBy(xpath = "//a[text()='News']")
	WebElement newsLink;
	
	@FindBy(xpath = "//a[text()='Surveys']")
	WebElement surveyLink;
	
	@FindBy(xpath = "//h1[text()='Useful Links']")
	WebElement usefulLinkPage;
	
	@FindBy(xpath = "//h1[text()='News']")
	WebElement newsLinkPage;
	
	@FindBy(xpath = "//h1[text()='Surveys']")
	WebElement surveysLinkPage;
	
	
	@FindBy(xpath = "//*[contains(@id, '_ClientUsefulLink_btnBack')]")
	WebElement closeButtonUseful;
	
	@FindBy(xpath = "//*[contains(@id, '_newsboard_btnBack')]")
	WebElement closeButtonNews;
	
	
	
	
	
	//Initializing the page objects
	public MainMenuOther(){
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void clickMainMenuLink(){
		try {
			clickButton(mainMenu);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void clickusefulLink(){
		try {
			clickButton(usefulLink);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void validateusefulLinkPage(){
		CommonFunctions.Validate(usefulLinkPage, "Useful Links");
	}
	
	public void closeButton(){
		try {
			clickButton(closeButtonUseful);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clicknewsLink(){
		try {
			clickButton(newsLink);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void validatenewsPage(){
		CommonFunctions.Validate(newsLinkPage, "News");
	} 
	
	public void closeButtonNews(){
		try {
			clickButton(closeButtonNews);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clicksurveysLink(){
		try {
			clickButton(surveyLink);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void validatesurveyLinkPage(){
		CommonFunctions.Validate(surveysLinkPage, "Surveys");
	}

}
