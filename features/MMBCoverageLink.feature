Feature: Verify features of Main Menu in EE Portal Home page

@MainMenu
Scenario: Verify the page elements of Your Coverage Information section
Given user launched the browser
Then user enter username and password
Then user is on the employee home page
And user click on the main menu button
When user click on the dental link button
Then dental page should display

@MainMenu
Scenario: Verify the page elements of InPatient page
Given user click on the main menu button
When user click on the inpatient link button
Then inpatient page should display


@MainMenu
Scenario: Verify the page elements of Maternity page
Given user click on the main menu button
When user click on the maternity link button
Then maternity page should display


@MainMenu
Scenario: Verify the page elements of list of coverages page
Given user click on the main menu button
When user click on the list of coverages link button
Then list of coverages page should display
Then close browser

