$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/MMBCoverageLink.feature");
formatter.feature({
  "line": 1,
  "name": "Verify features of Main Menu in EE Portal Home page",
  "description": "",
  "id": "verify-features-of-main-menu-in-ee-portal-home-page",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Verify the page elements of Your Coverage Information section",
  "description": "",
  "id": "verify-features-of-main-menu-in-ee-portal-home-page;verify-the-page-elements-of-your-coverage-information-section",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@MainMenu"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username and password",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user is on the employee home page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user click on the main menu button",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user click on the dental link button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "dental page should display",
  "keyword": "Then "
});
formatter.match({
  "location": "SelectSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 5244766613,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.enter_username_and_password()"
});
formatter.result({
  "duration": 63136977021,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.employee_portal_Home_Page()"
});
formatter.result({
  "duration": 36894591670,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_main_menu_button()"
});
formatter.result({
  "duration": 5126669538,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_dental_link_button()"
});
formatter.result({
  "duration": 5872370773,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.dental_page_should_display()"
});
formatter.result({
  "duration": 3293374036,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Verify the page elements of InPatient page",
  "description": "",
  "id": "verify-features-of-main-menu-in-ee-portal-home-page;verify-the-page-elements-of-inpatient-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@MainMenu"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "user click on the main menu button",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "user click on the inpatient link button",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "inpatient page should display",
  "keyword": "Then "
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_main_menu_button()"
});
formatter.result({
  "duration": 5379206833,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_inpatient_link_button()"
});
formatter.result({
  "duration": 5892250280,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.inpatient_page_should_display()"
});
formatter.result({
  "duration": 3127253011,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Verify the page elements of Maternity page",
  "description": "",
  "id": "verify-features-of-main-menu-in-ee-portal-home-page;verify-the-page-elements-of-maternity-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 19,
      "name": "@MainMenu"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "user click on the main menu button",
  "keyword": "Given "
});
formatter.step({
  "line": 22,
  "name": "user click on the maternity link button",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "maternity page should display",
  "keyword": "Then "
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_main_menu_button()"
});
formatter.result({
  "duration": 5264125022,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_maternity_link_button()"
});
formatter.result({
  "duration": 5959627114,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.maternity_page_should_display()"
});
formatter.result({
  "duration": 3109216690,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Verify the page elements of list of coverages page",
  "description": "",
  "id": "verify-features-of-main-menu-in-ee-portal-home-page;verify-the-page-elements-of-list-of-coverages-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@MainMenu"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "user click on the main menu button",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "user click on the list of coverages link button",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "list of coverages page should display",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_main_menu_button()"
});
formatter.result({
  "duration": 5267689810,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.user_click_on_the_list_of_coverages_link_button()"
});
formatter.result({
  "duration": 5893472888,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.list_of_coverages_page_should_display()"
});
formatter.result({
  "duration": 1458223899,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.closeBrowser()"
});
formatter.result({
  "duration": 1247465271,
  "status": "passed"
});
});